package org.example;

import java.util.*;

public class Warmup {
    public ArrayList<Integer> sortArray(ArrayList<Integer> input){
        ArrayList<Integer> sortedArray = new ArrayList<>();

        if(input.isEmpty()){
            return sortedArray;
        }

        for (int i = 0; i < input.size()-1; i++) {
            for (int j = i + 1; j < input.size(); j++) {
                if(input.get(i)>input.get(j)){
                    int hold = input.get(j);
                    input.set(j,input.get(i));
                    input.set(i,hold);
                }
            }
        }

        return input;
    }
    private ArrayList<HashMap<String,List<Integer>>> listOfHashMaps = new ArrayList<>();
    public ArrayList<HashMap<String,List<Integer>>> addHashMapToArray(String name, Integer age){
        if(name.isEmpty()){
            return listOfHashMaps;
        }
        List<Integer> ages = new ArrayList<Integer>();
        HashMap<String, List<Integer>> temp = new HashMap<>();
        for (HashMap<String,List<Integer>> elm : listOfHashMaps) {
            if (elm.containsKey(name)){
                List<Integer> ages2 = (List<Integer>) elm.get(name);
                ages2.add(age);
                temp.put(name,ages2);
                listOfHashMaps.set(listOfHashMaps.indexOf(elm),temp);
                return listOfHashMaps;
            }
        }
        ages.add(age);
        temp.put(name, ages);
        listOfHashMaps.add(temp);

        return listOfHashMaps;
    }
    public ArrayList<String> whoAreMyPeople(ArrayList<HashMap<String, List<Integer>>> input){
        ArrayList<String> formatted = new ArrayList<String>();

        for(HashMap<String,List<Integer>> elm : input){
            StringBuilder temp = new StringBuilder("I know ");

            String currentName = elm.keySet().toString();

            currentName = currentName.substring(1,currentName.length()-1);
            int numberOfPeople = elm.get(currentName).size();

            Collection<List<Integer>> ages = elm.values();
            ArrayList<Integer> actualAges = new ArrayList<>();
            ages.forEach(actualAges::addAll);

            if(numberOfPeople == 1){
                temp.append( numberOfPeople + " person named " + currentName + " and their age is " + actualAges.get(0) + ".");
            }else{
                temp.append( numberOfPeople + " people named " + currentName + " and their ages are ");
                for (int i = 0; i < actualAges.size() ; i++) {
                    if(i==0){
                        temp.append(actualAges.get(i));
                    }else if(i == actualAges.size()-1){
                        temp.append(" and " + actualAges.get(i) + ".");
                    }else{
                        temp.append(", " + actualAges.get(i));
                    }
                }
            }
            System.out.println(temp);
            formatted.add(String.valueOf(temp));
        }
        return formatted;
    }
}
