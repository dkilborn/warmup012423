package org.example;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarmupTest {

    @Test
    public void sortArrayShouldSortAnArrayListOfIntegersNumerically(){
        Warmup myWarmup = new Warmup();
        ArrayList<Integer> test1 = new ArrayList<>(Arrays.asList(5,4,3,2,1));
        ArrayList<Integer> test2 = new ArrayList<>(Arrays.asList(8,1,4,2,9,6));
        ArrayList<Integer> test3 = new ArrayList<>();

        ArrayList<Integer> result = myWarmup.sortArray(test1);
        ArrayList<Integer> result2 = myWarmup.sortArray(test2);
        ArrayList<Integer> result3 = myWarmup.sortArray(test3);

        assertEquals("[1, 2, 3, 4, 5]",result.toString());
        assertEquals("[1, 2, 4, 6, 8, 9]",result2.toString());
        assertEquals("[]",result3.toString());
    }

    @Test
    public void addHashMapToArrayShouldReturnAnArrayListOfHashMaps(){
        //-----TEST CASE 1-----
        Warmup myWarmup = new Warmup();

        ArrayList<HashMap<String, List<Integer>>> result = myWarmup.addHashMapToArray("Mike",30);
        assertEquals("[{Mike=[30]}]", result.toString());

        myWarmup.addHashMapToArray("Dylan",26);
        myWarmup.addHashMapToArray("Xavier",28);
        result = myWarmup.addHashMapToArray("Justin",32);

        assertEquals("[{Mike=[30]}, {Dylan=[26]}, {Xavier=[28]}, {Justin=[32]}]", result.toString());

        //-------TEST CASE 2-------
        Warmup myWarmup2 = new Warmup();

        myWarmup2.addHashMapToArray("Jimmy",21);
        ArrayList<HashMap<String, List<Integer>>> result2 = myWarmup2.addHashMapToArray("Jimmy",35);
        assertEquals("[{Jimmy=[21, 35]}]", result2.toString());

        myWarmup2.addHashMapToArray("Johnny",33);
        myWarmup2.addHashMapToArray("Timmy",40);
        result2 = myWarmup2.addHashMapToArray("Johnny",40);

        assertEquals("[{Jimmy=[21, 35]}, {Johnny=[33, 40]}, {Timmy=[40]}]", result2.toString());

        //------TEST CASE 3--------
        Warmup myWarmup3 = new Warmup();

        ArrayList<HashMap<String, List<Integer>>> result3 = myWarmup3.addHashMapToArray("",0);

        assertEquals("[]",result3.toString());

        myWarmup3.addHashMapToArray("Johnny", 50);
        myWarmup3.addHashMapToArray("James",33);
        myWarmup3.addHashMapToArray("Johnny",33);
        result3 = myWarmup3.addHashMapToArray("",0);

        assertEquals("[{Johnny=[50, 33]}, {James=[33]}]",result3.toString());

    }

    @Test
    public void whoAreMyPeopleShouldReturnAFormattedArrayListOfStrings(){
        //Write a method called whoAreMyPeople that accepts an ArrayList of HashMaps
        // as an argument. It should then return an ArrayList of Strings that
        // read "I know X number of name  and they are age and age years old"
        // For bonus points make it grammatically correct

        Warmup myWarmup = new Warmup();
        myWarmup.addHashMapToArray("Jimmy", 21);
        myWarmup.addHashMapToArray("Johnny", 22);
        myWarmup.addHashMapToArray("Johnny", 55);
        ArrayList<HashMap<String,List<Integer>>> testInput = myWarmup.addHashMapToArray("Johnny", 24);

        ArrayList<String> result = myWarmup.whoAreMyPeople(testInput);
    }
}
